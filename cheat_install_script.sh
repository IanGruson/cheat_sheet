#! /bin/bash
sudo apt update && sudo apt install fzf bat

SHELLRC="$HOME/.bashrc"
CHEATFOLDER="$(pwd)/cheatsheets"
if [[ -d $CHEATFOLDER ]]; then 
	mkdir -p  $CHEATFOLDER
fi

CHEAT_EXPORT="export CHEATFOLDER=$CHEATFOLDER"

if [[ -z $(grep $CHEAT_EXPORT $SHELLRC) ]]; then 
	echo $CHEAT_EXPORT >> $SHELLRC
fi

CONFIG_FOLDER="$HOME/.config"
if [[ ! $CONFIG_FOLDER -eq 0 ]]; then
	mdkir $CONFIG_FOLDER
fi
ALIASESRC="$CONFIG_FOLDER/.aliasesrc"

if [[ ! $ALIASESRC -eq 0 ]]; then
	touch $ALIASESRC
fi
echo "alias cheat=\"bat $CHEATFOLDER/*.md | fzf\"" >> $ALIASESRC

source $SHELLRC
