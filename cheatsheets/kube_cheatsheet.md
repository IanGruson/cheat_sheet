# Useful commands
kubectl port-forward deployment/[DEPLOYMENT_NAME] 5432:5432 //port forward postgres database 
kubectl top pod // get pods RAM Usage 
kubectl --exec -it <pod_name> -- /bin/bash // connect to the pod with a shell instance
