# Useful Commands

## Password
git config credential.helper store // store password on next interaction with remote to reduce number of time required to enter password 

## Alias
git config --global alias.co checkout // example to create a alias for checkout
